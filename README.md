Service implementing the airplay protocol to be used as a simulator to test airplay clients.
Protocol is based on https://nto.github.io/AirPlay.html.

Currently only the photo endpoint is supported.
