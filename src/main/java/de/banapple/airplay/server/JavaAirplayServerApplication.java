package de.banapple.airplay.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaAirplayServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaAirplayServerApplication.class, args);
	}
}
