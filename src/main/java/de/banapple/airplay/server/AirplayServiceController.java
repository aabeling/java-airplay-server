package de.banapple.airplay.server;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AirplayServiceController {

    /**
     * Returns the airplay service txt record
     * 
     * @throws IOException
     */
    @RequestMapping("/")
    public void getInfo(HttpServletResponse response) throws IOException {

        PrintWriter writer = response.getWriter();

        writer.println("name: java-airplay-server");
        writer.println("type: " + Registrar.SERVICE_TYPE);
        writer.println("port: " + 8080);
        writer.println("txt:");
        writer.println(" deviceid=mac-address-todo");
        /* only photo is supported */
        writer.println(" features=0x0002");
        writer.println(" model=java-airplay-server");
        writer.println(" srcvers=130.14");

    }
}
