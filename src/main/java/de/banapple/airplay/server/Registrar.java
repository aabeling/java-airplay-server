package de.banapple.airplay.server;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.jmdns.JmDNS;
import javax.jmdns.ServiceInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Responsible for registering this service as an airplay service.
 */
@Component
public class Registrar {

    private static final Logger LOG = LoggerFactory.getLogger(Registrar.class);

    public static final String SERVICE_TYPE = "_airplay._tcp.local.";

    private ServiceInfo info;
    
    @PostConstruct
    public void register() {

        try {

            info = serviceInfo();
            JmDNS jmDns = JmDNS.create();
            jmDns.registerService(info);

            LOG.debug("registered service: {}", info);

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @PreDestroy
    public void deregister() {

        JmDNS jmDns;
        try {
            jmDns = JmDNS.create();
            jmDns.unregisterService(info);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private ServiceInfo serviceInfo() {

        return ServiceInfo.create(
                SERVICE_TYPE,
                "java-airplay-server",
                8080,
                "this is the java-airplay-server");
    }
}
