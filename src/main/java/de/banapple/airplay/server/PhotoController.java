package de.banapple.airplay.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Responsible for handling photo updates.
 */
@Controller
public class PhotoController {

    private static final Logger LOG = LoggerFactory.getLogger(PhotoController.class);

    private static final String FILENAME_CURRENT_PHOTO = "airplay-server-photo.jpg";

    /**
     * Sets the current photo.
     * 
     * @param request
     * @param response
     * @throws IOException
     */
    @RequestMapping(method = RequestMethod.PUT, path = "/photo")
    public void updatePhoto(
            final HttpServletRequest request,
            final HttpServletResponse response) throws IOException {

        LOG.debug("updating photo");

        File file = new File(FILENAME_CURRENT_PHOTO + ".tmp");
        FileOutputStream output = new FileOutputStream(file);
        IOUtils.copyLarge(request.getInputStream(), output);
        output.close();

        File currentPhoto = new File(FILENAME_CURRENT_PHOTO);
        file.renameTo(currentPhoto);

        response.setStatus(200);
    }

    /**
     * Returns the current photo.
     * 
     * @param response
     * @throws IOException
     */
    @RequestMapping(method = RequestMethod.GET, path = "/photo")
    public void getPhoto(
            final HttpServletResponse response) throws IOException {

        File currentPhoto = new File(FILENAME_CURRENT_PHOTO);
        FileInputStream input = new FileInputStream(currentPhoto);
        IOUtils.copyLarge(input, response.getOutputStream());
        
        response.setStatus(200);
    }

}
